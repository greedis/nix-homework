<?php
/**
 * @param $value
 * @return void
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function printR($value):void{
    if (is_int($value) || is_double($value) || is_float($value)){
        echo $value;
        return;
    }
    if (is_bool($value)){
        echo $value ? 'true' : 'false';
        return;
    }
    if (is_array($value)){
        $i = 1;
        echo 'Array' . ' ( ';
        foreach ($value as $key => $item){
            if(is_array($item)) printR($item);
            else echo '[' . $key . ']' . ' => ' . $item . ($i++ == count($value) ? '' : ' ') . ' ';
        }
        echo ' )';
    }
    else echo $value . (gettype($value) == 'string' ? '{' . strlen($value) . '}' : '') . ': ' . $value;
}

$arr = array(80, "ewf", 100, 10, 50, 3);

printR($arr);
