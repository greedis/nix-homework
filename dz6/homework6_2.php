<?php
/**
 * @param $array
 * @return void
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function dd($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

/**
 * @param int $arrLength
 * @param int $min
 * @param int $max
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function fillTheArray(int $arrLength, int $min = 0, int $max = 100): array
{
    $arr = [];
    for ($i = 0; $i < $arrLength; $i++) {
        $arr[] = rand($min, $max);
    }
    return $arr;
}
//Task 1
echo 'TASK 1';
echo '<br>';
/**
 * @param int $n
 * @param int $p
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getNumberInPower(int $n, int $p):int{
    return $n**$p;
}
$getNumberInPowerAnon = function (int $n, int $p):int{
  return $n**$p;
};
$getNumberInPowerArrow = fn (int $n, int $p):int => $n**$p;
$number = 4;
$power = 3;
echo 'Number ' . $number . ' in power of ' . $power . '= ' . getNumberInPower($number, $power);
//Task 2
echo '<br>';
echo 'TASK 2';

/**
 * @param array $arr
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function sortArrayAsc(array $arr):array{
    for ($count = 0; $arr[$count]; $count++) ;
        for($i = 0; $i < $count; $i++) {
            for($j = 0; $j < $count-1; $j++){
                if($arr[$j] > $arr[$j+1]) {
                    $key = $arr[$j+1];
                    $arr[$j+1]=$arr[$j];
                    $arr[$j]=$key;
                }
            }
        }
    return $arr;
}

/**
 * @param array $arr
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function sortArrayDesc(array $arr):array{
    for ($count = 0; $arr[$count]; $count++) ;
    for ($i = 0; $i < $count; $i++){
        $key = 0;
        for ($j = 0; $j< $count; $j++){
            if($arr[$j]<$arr[$i]){
                $key=$arr[$i];
                $arr[$i]=$arr[$j];
                $arr[$j]=$key;
            }
        }
    }
    return $arr;
}

/**
 * @param array $arr
 * @param string $param
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function sortArr(array $arr, string $param = 'asc') : array
{
    return $param == 'desc' ? sortArrayDesc($arr) : sortArrayAsc($arr);
}
$sortArrAnon = function (array $arr, string $param = 'asc') : array
{
    return $param == 'desc' ? sortArrayDesc($arr) : sortArrayAsc($arr);
};
$sortArrArrow = fn (array $arr, string $param = 'asc') : array => $param === 'desc' ? sortArrayDesc($arr) : sortArrayAsc($arr);
$arr = fillTheArray(20);
dd($arr);
$arr = sortArr($arr, 'asc');
dd($arr);
// Task 3
echo 'TASK 3';
/**
 * @param array $arr
 * @param int $find
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function search(array $arr, int $find):int{
    for ($count = 0; $arr[$count]; $count++);
    $num = 0;
    for ($i = 0; $i < $count; $i++){
        if ($arr[$i] == $find){
            $num++;
        }
    }
    return $num;
}
$searchAnon = function (array $arr, int $find):int{
    for ($count = 0; $arr[$count]; $count++);
    $num = 0;
    for ($i = 0; $i < $count; $i++){
        if ($arr[$i] == $find){
            $num++;
        }
    }
    return $num;
};
$arr = [1, 2, 3, 4, 5, 6, 7];
dd($arr);
$n = 5;
echo 'Count of ' . $n . ' in this array is ' . search($arr, $n);

