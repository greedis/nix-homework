<?php
/**
 * @param int $countryCoef
 * @param int $days
 * @param bool $discount
 * @return float
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function calculatePrice(int $countryCoef, int $days, bool $discount):float{
    $price = 400 * $days * $countryCoef * ($discount ? 0.95 : 1);
    return $price;
}
if ($_REQUEST['doGo']){
    if($_REQUEST['country'] = 'Egypt'){
        $coefficient = 1.1;
    } elseif ($_REQUEST['country'] = 'Turkey'){
        $coefficient = 1;
    } else $coefficient = 1.12;
    if ($_REQUEST['days']>0){
        $days = $_REQUEST['days'];
    } else echo 'Count of days must be more than 0';
    if ($_REQUEST['discount']){
        $discount = true;
    } else $discount = false;
    $price = calculatePrice($coefficient, $days, $discount);
    echo '<br>';
    echo 'Your price = ' . $price;
}




?>

<style>
    form {
        width: 300px;
    }
</style>

<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <fieldset>
        <legend>Enter your parameters</legend>
        <table>
            <tr>
                <td><label for=country></label>Country:</td>
            </tr>
            <tr>
                <td>
                    <select name="user_profile_color_1">
                        <option value="1">Egypt</option>
                        <option value="2">Turkey</option>
                        <option value="3">Italy</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for=days></label>Days:</td>
            </tr>
            <tr>
                <td><input id="days" name="days" type="number"></td>
            </tr>
            <tr>
                <td><label for=discount></label>Do you have a discount:</td>
            </tr>
            <tr>
                <td><input id="discount" name="yes" type="checkbox">Yes</td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <input type="submit" name="doGo" value="Calculate price">
        <input type="reset" name="reset" value="Reset"><br>
    </fieldset>
