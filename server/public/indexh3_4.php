<?php
/**
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
session_start();
?>

<style>
    form {
        width: 300px;
    }
</style>

<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <fieldset>
        <legend>Checkout</legend>
        <table>
            <tr>
                <td><label for=name></label>Name:</td>
            </tr>
            <tr>
                <td><input id="name" name="name" type="text" value="<?= $_SESSION['task3']['name']?>" REQUIRED></td>
            </tr>
            <tr>
                <td><label for=number></label>Number:</td>
            </tr>
            <tr>
                <td><input id="number" name="number" type="tel" value="<?= $_SESSION['task3']['number']?>" REQUIRED></td>
            </tr>
        </table>
        <table>
            <tr>
                <td><label for=tod></label>Type of delivery:</td>
            </tr>
            <tr>
                <td>
                    <input id="by_courier" name="tod" type="radio" value="By courier">
                    <label for="by_courier">By courier</label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="by_new_post" name="tod" type="radio" value="By New Post">
                    <label for="by_new_post">By New Post</label>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="by_ukr_post" name="tod" type="radio" value="By Ukr Post">
                    <label for="by_ukr_post">By Ukr Post</label>
                </td>
        </table>
        <table>
            <tr>
                <td><label for="address"></label>Address of delivery </td>
            </tr>
            <tr>
                <td> <textarea id="address" name="address" rows="5" cols="20"></textarea> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td><label for="productID">ID of your product:</label> </td>
            </tr>
            <tr>
                <td><input id="productID" name="productID" type="number" value="<?= $_SESSION['task2']['productID']?>"></td>
            </tr>
            <tr>
                <td><label for="count">Count of your product:</label> </td>
            </tr>
            <tr>
                <td><input id="count" name="count" type="number" value="<?= $_SESSION['task2']['count']?>"></td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <input type="submit" name="doGo" value="Checkout">
        <input type="reset" name="reset" value="Clear"><br>
    </fieldset>
<?php
if (@$_REQUEST['doGo']) {
    if (!$_SESSION['task4']) {
        $_SESSION['task4'] = array('name' => $_REQUEST['name'], 'number' => $_REQUEST['number'], 'tod' => $_REQUEST['tod'], 'address' => $_REQUEST['address'], 'productID' => $_REQUEST['productID'], 'count' => $_REQUEST['count']);
    } else {
        array_push($_SESSION['task4'], ['name' => $_REQUEST['name'], 'number' => $_REQUEST['number'], 'tod' => $_REQUEST['tod'], 'address' => $_REQUEST['address'], 'productID' => $_REQUEST['productID'], 'count' => $_REQUEST['count']]);
    }
    echo '<pre>';
    print_r($_SESSION['task4']);
    echo '</pre>';
}
?>