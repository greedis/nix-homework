<?php
/**
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homework for 03.12</title>
</head>
<body>
<h1>*</h1>
<ol>
    <li><?php echo(42 > 55 ? '42 is greater' : '55 is greater'); ?></li>
    <li><?php
        $n1 = rand(-10, 10);
        $n2 = rand(-10, 10);
        echo($n1 > $n2 ? "$n1 is greater" : "$n2 is greater");
        ?>
    </li>
    <li>
        <?php
        $surname = 'Dolgoy';
        $name = 'Ilya';
        $pastName = 'Maksimovych';
        echo "$surname $name $pastName";
        echo "<br>";
        echo "$surname $name[0]. $pastName[0].";
        ?>
    </li>
    <li>
        <?php
        $n = rand(1, 99999);
        $s = (string)$n;
        $c = '7';
        echo "Count $c in $s = " . substr_count(
                $s,
                '7',
                0,
                strlen($s)
            )
        ?>
    </li>
    <li>
        <?php
        $a = 3;
        echo "a) $a";
        echo "<br>";
        $a = 10;
        $b = 2;
        echo 'b) a + b = ' . ($a + $b) . ', a - b = ' . ($a - $b) . ', a * b = ' . ($a * $b) . ', a / b = ' . ($a / $b);
        echo "<br>";
        $c = 15;
        $d = 2;
        $result = $c + $d;
        echo "c) c + d = $result";
        echo "<br>";
        $a = 10;
        $b = 2;
        $c = 5;
        echo 'd) a + b + c = ' . ($a + $b + $c);
        echo "<br>";
        $a = 17;
        $b = 10;
        $c = $a + $b;
        $d = 5;
        $result = $c + $d;
        echo "e) $result";
        ?>
    </li>
    <li>
        <?php
        $text = 'Привет, мир!';
        echo "a) $text";
        echo "<br>";
        $text1 = 'b) Привет,';
        $text2 = 'Мир!';
        echo $text1 . $text2;
        echo "<br>";
        $countInMin = 60;
        $countInHour = ($countInMin * 60);
        $countInDay = ($countInHour * 24);
        $countInWeek = ($countInDay * 7);
        $countInMonth = ($countInDay * 30);
        echo "c) Seconds in hour = $countInHour";
        echo ",  Seconds in the day = $countInDay";
        echo ", Seconds in the week = $countInWeek";
        echo ", Seconds in month = $countInMonth";
        ?>
    </li>
    <li>
        <?php
        $var = 1;
        $var += 12;
        $var -= 14;
        $var *= 5;
        $var /= 7;
        $var %= 1;
        echo $var;
        ?>
    </li>
</ol>
<h1>**</h1>
<ol>
    <li>
        <?php
        $hour = 01;
        $minute = 06;
        $second = 17;
        echo "Time = $hour:$minute:$second";
        ?>
    </li>
    <li>
        <?php
        $text = 'Я ';
        $text .= 'хочу ';
        $text .= 'знать ';
        $text .= 'PHP!';
        echo $text;
        ?>
    </li>
    <li>
        <?php
        $foo = 'bar';
        $bar = 10;
        echo $$foo;
        ?>
    </li>
    <li>
        <?php
        echo 'Какой будет результат если: $a = 2; $b = 4; echo $a++ + $b; echo $a + ++$b; echo ++$a + $b++;';
        echo '<br>';
        echo 'Результат: 6, 8, 9';
        ?>
    </li>
    <li>
        <?php
        $i = 5;
        echo 'a) ' . (isset($i) ? 'Variable is set' : 'Variable is\'nt set');
        echo '<br>';
        echo 'b) ' . (gettype($i) == 'integer' ? 'Variable is integer' : 'Variable is not integer');
        echo '<br>';
        echo 'c) ' . (is_null($i) ? 'Variable is null' : 'Variable is not null');
        echo '<br>';
        echo 'd) ' . (empty($i) ? 'Variable is empty' : 'Variable is not empty');
        echo '<br>';
        echo 'e) ' . (is_integer($i) ? 'Variable is integer' : 'Variable is not integer');
        echo '<br>';
        echo 'f) ' . (is_double($i) ? 'Variable is double' : 'Variable is not double');
        echo '<br>';
        echo 'g) ' . (is_string($i) ? 'Variable is string' : 'Variable is not string');
        echo '<br>';
        echo 'h) ' . (is_numeric($i) ? 'Variable is numeric' : 'Variable is not numeric');
        echo '<br>';
        echo 'i) ' . (is_bool($i) ? 'Variable is bool' : 'Variable is not bool');
        echo '<br>';
        echo 'j) ' . (is_scalar($i) ? 'Variable is scalar' : 'Variable is not scalar');
        echo '<br>';
        echo 'k) ' . (is_array($i) ? 'Variable is array' : 'Variable is not array');
        echo '<br>';
        echo 'l) ' . (is_object($i) ? 'Variable is object' : 'Variable is not object');
        echo '<br>';

        ?>
    </li>
    <li>
        <?php
        echo 'Какой будет результат если: $a = 2; $b = 4; echo $a++ + $b; echo $a + ++$b; echo ++$a + $b++;';
        echo '<br>';
        echo 'Результат: 6, 8, 9';
        ?>
    </li>
</ol>
<h1>***</h1>
<ol>
    <li>
        <?php
        $a = 13;
        $b = 19;
        echo "a = $a, b = $b, a + b = " . ($a + $b) . ', a * b = ' . ($a * $b);
        ?>
    </li>
    <li>
        <?php
        $a = 14;
        $b = 18;
        echo 'a^2 + b^2 = ' . (pow($a, 2) + pow($b, 2));
        ?>
    </li>
    <li>
        <?php
        $a = 13;
        $b = 19;
        $c = 22;
        echo "a = $a, b = $b, c = $c";
        echo '<br>';
        echo 'Average of this numbers = ' . (($a + $b + $c) / 3);
        ?>
    </li>
    <li>
        <?php
        $x = 3;
        $y = 9;
        $z = 2;
        echo ($x + 1) - 2 * ($z - 2 * $x + $y);
        ?>
    </li>
    <li>
        <?php
        $a = 22;
        echo "a = $a";
        echo '<br>';
        echo 'Остаток от деления на 5 = ' . ($a % 5) . ', Остаток от деления на 3 = ' . ($a % 3);
        echo '<br>';
        echo '30% of a = ' . ($a * 0.3) . ', 120% of a = ' . ($a * 1.2);
        ?>
    </li><li>
        <?php
        $a = 22;
        $b = 39;
        $c = rand(100, 999);
        echo "a = $a";
        echo '<br>';
        echo "b = $b";
        echo '<br>';
        echo "c = $c";
        echo '<br>';
        echo 'Sum of 40% of a and 84% of b = ' . (($a * 0.4) + ($b * 0.84));
        echo '<br>';
        $c = (string)$c;
        echo 'Sum of c numbers = ' . ($c[0] + $c[1] + $c[2]);
        ?>
    </li><li>
        <?php
        $c = rand(100, 999);
        echo "c = $c";
        echo '<br>';
        $c = (string)$c;
        $c[1] = 0;
        echo $c;
        echo '<br>';
        echo $c[2] . $c[1] . $c[0];
        ?>
    </li><li>
        <?php
        $c = rand(1,100);
        echo "c = $c";
        echo '<br>';
        echo $c % 2 == 0 ? 'Число четное' : 'Число не четное';

        ?>
    </li>
</ol>
</body>
</html>