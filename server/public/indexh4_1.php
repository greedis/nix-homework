<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homework for 10.12</title>
</head>
<body>
<ol>
    <li><?php
        $n = 100;
        echo 'Prime numbers: ';
        for ($i = 2; $i <= $n; $i++) {
            for ($j = 2; $j < $i; $j++) {
                if ($i % $j == 0) continue 2;
            }
            echo $i . ' ';
        }?></li>
    <li><?php $arr = array();
        echo '100 random numbers:';
        echo '<br>';
        for ($i=0; $i<=100; $i++){
            $x = rand(0, 50);
            echo $x . '|';
            if($x % 2 == 0){
                $arr[] = $x;
            }
        }
        echo '<br>' . 'Count of even numbers: ';
        echo count($arr);?></li>
    <li><?php
        $arr1 = array();
        $arr2 = array();
        $arr3 = array();
        $arr4 = array();
        $arr5 = array();
        echo '100 random numbers:';
        echo '<br>';
        for ($i=0; $i<=100; $i++){
            $x = rand(1, 5);
            echo $x . '|';
            switch ($x) {
                case 1:
                    $arr1[] = $x;
                    break;
                case 2:
                    $arr2[] = $x;
                    break;
                case 3:
                    $arr3[] = $x;
                    break;
                case 4:
                    $arr4[] = $x;
                    break;
                case 5:
                    $arr5[] = $x;
                    break;
            }
        }
        echo '<br>' . 'Count of 1 numbers: ';
        echo count($arr1);
        echo '<br>' . 'Count of 2 numbers: ';
        echo count($arr2);
        echo '<br>' . 'Count of 3 numbers: ';
        echo count($arr3);
        echo '<br>' . 'Count of 4 numbers: ';
        echo count($arr4);
        echo '<br>' . 'Count of 5 numbers: ';
        echo count($arr5);?></li>
    <li><?php
        $b = 0;
$c = 0;
echo "<table border='1'";
for ($a = 0; $a < 153; $a += 51) {
    echo "<tr>";
//    for ($b = 0; $b < 256; $b += 51) {
    for ($c = 0; $c < 205; $c += 51) {
        echo "<td style=background-color:rgb($a,$b,$c);>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
    }
//    }
}
echo "</tr>";
echo "</table>"; ?></li>
    <li><?php
        $month = rand(1, 12);
        echo "Month - $month";
        echo '<br>';
        switch ($month){
            case ($month>=3 && $month<=5):
                echo 'It`s spring!';
                break;
            case ($month>=6 && $month<=8):
                echo 'It`s summer!';
                break;
            case ($month>=9 && $month<=11):
                echo 'It`s autumn!';
                break;
            default:
                echo 'It`s winter!';
                break;
        }?></li>
    <li><?php
        $a = 'abcde';
        echo 'Строка - ' . $a;
        echo '<br>';
        echo 'Эта строка начинается на a?';
        echo '<br>';
        if ($a{0} = 'a'){
            echo 'Да';
        }else echo 'Нет';?></li>
    <li><?php
        $a = '12345';
        echo 'Строка - ' . $a;
        echo '<br>';
        echo 'Эта строка начинается на 1, 2 или 3?';
        echo '<br>';
        if ($a{0} >=1 && $a{0} <=3){
            echo 'Да';
        } else echo 'Нет';?></li>
    <li><?php
        $test = false;
        echo "Test - $test";
        echo '<br>';
        echo $test ? 'Верно' : 'Неверно';?></li>
    <li><?php
        $test = true;
        echo "Test - $test";
        echo '<br>';
        if ($test){
            echo 'Верно';
        } else echo 'Неверно';?></li>
    <li><?php
        $russWeek = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресение', ];
        $engWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', ];
        $lang1 = 'en';
        echo $lang1;
        if ($lang1 == 'ru'){
            echo '<pre>';
            print_r($russWeek);
            echo '</pre>';
        } elseif ($lang1 == 'en'){
            echo '<pre>';
            print_r($engWeek);
            echo '</pre>';
        }?></li>
    <li><?php
        $clock = rand(0, 59);
        echo " Minute $clock";
        echo '<br>';
        echo (($clock>=0 && $clock<=15) ? '1st quarter' :(($clock>=16 && $clock<=30) ? '2nd quarter' :(($clock>=31 && $clock<=45) ? '3rd quarter' : '4th quarter')))
        ?></li>
    <li><?php
        $clock = rand(0, 59);
        echo " Minute $clock";
        echo '<br>';
        if ($clock>=0 && $clock<=15){
            echo '1st quarter';
        } elseif ($clock>=16 && $clock<=30){
            echo '2nd quarter';
        } elseif ($clock>=31 && $clock<=45){
            echo '3rd quarter';
        } else echo '4th quarter';

        ?></li>
</ol>
</body>
</html>