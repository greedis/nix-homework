<?php
/**
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
session_start();
?>
<style>
    form {
        width: 300px;
    }
</style>

<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <fieldset>
        <legend>Let your contacts</legend>
        <table>
            <tr><td><label for=name></label>Name: </td></tr>
            <tr><td><input id="name" name="name" type="text" REQUIRED></td></tr>
            <tr><td><label for=number></label>Phone number: </td></tr>
            <tr><td><input id="number" name="number" type="tel" REQUIRED></td></tr>
        </table>
    </fieldset>
    <fieldset>
        <input type="submit" name="doGo" value="Отправить">
        <input type="reset" name="reset" value="Очистить"><br>
    </fieldset>

<?php
if (@$_REQUEST['doGo']) {
    if (!$_SESSION['task3']) {
        $_SESSION['task3'] = array('name' => $_REQUEST['name'], 'number' => $_REQUEST['number']);
    } else {
        array_push($_SESSION['task3'], ['name' => $_REQUEST['name'], 'number' => $_REQUEST['number']]);
    }
    echo '<pre>';
    print_r($_SESSION['task3']);
    echo '</pre>';
}
?>