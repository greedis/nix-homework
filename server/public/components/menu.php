<header
    class="header"
    style="
        padding: 10px;
        margin: 10px;
        border-bottom: 1px solid #000"
>
    <nav class="navigation">
        <ul
            class="navigation__list"
            style="
                list-style-type: none;
                margin: 0;
                display: flex"
        >
            <li class="navigation__elem" style="margin-left: 20px">
                <a href="/file.php" class="navigation__link">
                    File upload
                </a>
            </li>
            <li class="navigation__elem" style="margin-left: 20px">
                <a href="/order.php" class="navigation__link">
                    Place order
                </a>
            </li>
            <li class="navigation__elem" style="margin-left: 20px">
                <a href="/product.php" class="navigation__link">
                    Add item to shopping cart
                </a>
            </li>
            <li class="navigation__elem" style="margin-left: 20px">
                <a href="/contact.php" class="navigation__link">
                    Contact
                </a>
            </li>
            <li class="navigation__elem" style="margin-left: 20px">
                <a href="/comment.php" class="navigation__link">
                    Comments
                </a>
            </li>
        </ul>
    </nav>
</header>
