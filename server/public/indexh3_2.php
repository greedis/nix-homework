<?php
/**
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
session_start();
?>
<style>
    form {
        width: 300px;
    }
</style>

<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <fieldset>
        <legend>Add a product to the basket</legend>
        <table>
            <tr>
                <td><label for=productID></label>Product ID:</td>
            </tr>
            <tr>
                <td><input id="productID" name="productID" type="number" REQUIRED></td>
            </tr>
            <tr>
                <td><label for=count></label>Count:</td>
            </tr>
            <tr>
                <td><input id="count" name="count" type="number" REQUIRED></td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <input type="submit" name="doGo" value="Отправить">
        <input type="reset" name="reset" value="Очистить"><br>
    </fieldset>
<?php
if (@$_REQUEST['doGo']) {
    if (!$_SESSION['task2']) {
        $_SESSION['task2'] = array('productID' => $_REQUEST['productID'], 'count' => $_REQUEST['count']);
    } else {
        array_push($_SESSION['task2'], ['productID' => $_REQUEST['productID'], 'count' => $_REQUEST['count']]);
    }
    echo '<pre>';
    print_r($_SESSION['task2']);
    echo '</pre>';
}
?>