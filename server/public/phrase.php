<?php
/**
 * @param int $count
 * @return void
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function repeatPhrase(int $count){
    for ($i = 1; $i<=$count; $i++){
        echo 'Silence is golden' . '<br>';
    }
}
if ($_REQUEST['doGo']){
   if ($_REQUEST['number'] > 0){
       $count = $_REQUEST['number'];
   } else{
       echo 'Bad n';
   }
   echo repeatPhrase($count);
}




?>

<style>
    form {
        width: 300px;
    }
</style>

<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <fieldset>
        <legend>Enter a number</legend>
        <table>
            <tr>
                <td><label for=number></label>N:</td>
            </tr>
            <tr>
                <td><input id="number" name="number" type="number"></td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <input type="submit" name="doGo" value="Send">
        <input type="reset" name="reset" value="Reset"><br>
    </fieldset>