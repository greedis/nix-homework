<?php
echo 'TASK 1';
/**
 * @param $array
 * @return void
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function dd($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

// Task 1
/**
 * @param int $arrLength
 * @param int $min
 * @param int $max
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function fillTheArray(int $arrLength, int $min = 0, int $max = 100): array
{
    $arr = [];
    for ($i = 0; $i < $arrLength; $i++) {
        $arr[] = rand($min, $max);
    }
    return $arr;
}

/**
 * @param array $array
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function multiplyElements(array $array): int
{
    $multiply = $array[0];
    for ($i = 0; $i < count($array); $i++) {
        if ($array[$i] > 0 && $i % 2 == 0) {
            $multiply = $multiply * $array[$i];
        }
    }
    $multiply = $multiply / $array[0];
    return $multiply;
}

/**
 * @param array $array
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function returnElements(array $array): array
{
    $arr = [];
    for ($i = 0; $i < count($array); $i++) {
        if ($array[$i] > 0 && $i % 2 != 0) {
            $arr[] = $array[$i];
        }
    }
    return $arr;
}
$arr = fillTheArray(10);
dd($arr);
$num = multiplyElements($arr);
echo $num;
$arr1 = returnElements($arr);
dd($arr1);
echo 'TASK 2';
/**
 * @param int $num1
 * @param int $num2
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function sum(int $num1, int $num2): int
{
    return $num1 + $num2;
}

/**
 * @param int $num1
 * @param int $num2
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function multiply(int $num1, int $num2): int
{
    return $num1 * $num2;
}

/**
 * @param int $num1
 * @param int $num2
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function sumOfSquares(int $num1, int $num2): int
{
    return ($num1 * $num1) + ($num2 * $num2);
}

echo '<br>';
echo sum(3, 3);
echo '<br>';
echo multiply(3, 3);
echo '<br>';
echo sumOfSquares(3, 3);
echo '<br>';
echo 'TASK 3';
/**
 * @param int $num1
 * @param int $num2
 * @param int $num3
 * @return float
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function avg(int $num1, int $num2, int $num3): float
{
    return ($num1 + $num2 + $num3) / 3;
}

echo '<br>';
echo 'AVG = ' . avg(3, 4, 5);
echo "<br>";
echo 'TASK 4';
/**
 * @param int $num
 * @return float
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function increaseOn30(int $num): float
{
    return $num + ($num * 0.3);
}

/**
 * @param int $num
 * @return float
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function increaseOn120(int $num): float
{
    return $num + ($num * 1.2);
}

echo '<br>';
echo 'Num increase on 30% = ' . increaseOn30(10);
echo '<br>';
echo 'Num increase on 120% = ' . increaseOn120(10);
// Task 8
echo '<br>';
echo 'TASK 3';
echo '<br>';
/**
 * @param int $length
 * @param array $array
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function fillArrayByn(int $length, array $array): array
{
    for ($i = 0; $i < $length; $i++) {
        if ($i % 2 == 0) {
            $array[$i] = 0;
        } else {
            $array[$i] = 1;
        }
    }
    return $array;
}

$arr = fillArrayByn(10, $arr);
dd($arr);
//Task 9
echo 'TASK 9';
echo '<br>';
/**
 * @param array $array
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function repeatValues(array $array): int
{
    $n = 0;
    for ($i = 0; $i < count($array) - 1; $i++) {
        for ($j = $i + 1; $j < count($array); $j++) {
            if ($array[$i] == $array[$j]) {
                $n++;
            }
        }
    }
    return $n;
}

$arr = fillTheArray(20);
$n = repeatValues($arr);
dd($arr);
echo 'There are ' . $n . ' repeat elements in the array';
echo '<br>';
//Task 10
echo 'TASK 10';
echo '<br>';
/**
 * @param int ...$values
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getMin(int ...$values): int
{
    $min = $values[0];
    foreach ($values as $value) {
        if ($value < $min) $min = $value;
    }
    return $min;
}

/**
 * @param int ...$values
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getMax(int ...$values): int
{
    $max = $values[0];
    foreach ($values as $value) {
        if ($value > $max) $max = $value;
    }
    return $max;
}

echo 'Values = 5, 3, 8';
echo '<br>';
echo 'Min = ' . getMin(5, 3, 8);
echo '<br>';
echo 'Max = ' . getMax(5, 3, 8);
echo '<br>';
//Task 11
echo 'TASK 11';
echo '<br>';
/**
 * @param int $a
 * @param int $b
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getSquare(int $a, int $b): int
{
    return $a * $b;
}

$a = 3;
$b = 4;
echo 'Length = ' . $a . ' , width = ' . $b;
echo '<br>';
echo 'Square = ' . getSquare($a, $b);
echo '<br>';
//Task 12
echo 'TASK 12';
echo '<br>';
/**
 * @param int $leg1
 * @param int $leg2
 * @return float
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getHypotenuse(int $leg1, int $leg2): float
{
    return sqrt(($leg1 * $leg1) + ($leg2 * $leg2));
}

$leg1 = 6;
$leg2 = 8;
echo 'Leg 1 = ' . $leg1 . ' , leg 2 = ' . $leg2;
echo '<br>';
echo 'Hypotenuse = ' . getHypotenuse($leg1, $leg2);
echo '<br>';
//Task 13
echo 'TASK 13';
echo '<br>';
/**
 * @param int $a
 * @param int $b
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getPerimeter(int $a, int $b): int
{
    return ($a + $b) * 2;
}

$a = 5;
$b = 6;
echo 'Length = ' . $a . ' , width = ' . $b;
echo '<br>';
echo 'Perimeter = ' . getPerimeter($a, $b);
echo '<br>';
//Task 14
echo 'TASK 14';
echo '<br>';
/**
 * @param int $a
 * @param int $b
 * @param int $c
 * @return int
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function getDiscriminant(int $a, int $b, int $c): int
{
    return ($b * $b) - 4 * ($a * $c);
}
$a = -5;
$b = 6;
$c = 7;
echo 'a = ' . $a . ' , b = ' . $b . ' , c = ' . $c;
echo '<br>';
echo 'Discriminant = ' . getDiscriminant($a, $b, $c);
echo '<br>';
//Task 15
echo 'TASK 15';
echo '<br>';
/**
 * @param int $length
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function createEvenArray(int $length):array{
    for ($i = 0; $i <= $length; $i++){
        if ($i%2 == 0){
            $arr[] = $i;
        }
    }
    return $arr;
}
$arr = createEvenArray(100);
dd($arr);
//Task 16
echo 'TASK 16';
echo '<br>';
/**
 * @param int $length
 * @return array
 * @author Dolgoy Ilya dolghoi.2002@gmail.com
 */
function createNotEvenArray(int $length):array{
    for ($i = 0; $i <= $length; $i++){
        if ($i%2 != 0){
            $arr[] = $i;
        }
    }
    return $arr;
}
$arr = createNotEvenArray(100);
dd($arr);