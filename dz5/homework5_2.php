<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homework for 10.12</title>
</head>
<body>
<ol>
    <li>
        <ul>
            <li>while:
                <?php
                $array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
                $count = 0;
                while ($array[$count]) {
                    $count++;
                }
                echo $count;
                ?>
            </li>
            <li>do while:
                <?php
                $array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
                $count = 0;
                do {
                    $count++;
                } while ($array[$count]);
                echo $count;
                ?>
            </li>
            <li> for:
                <?php
                $array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
                for ($count = 0; $array[$count]; $count++) ;
                echo $count;
                ?>
            </li>
            <li>foreach:
                <?php
                $array = ['Alex', 'Kostya', 'Pavel', 'Oleg'];
                $count = 0;
                foreach ($array as $val) {
                    $count++;
                }
                echo $count; ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count;
                while ($i >= 0) {
                    $i--;
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>do while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count;
                do {
                    $i--;
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                } while ($i >= 0);
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li> for:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                for ($i = $count - 1; $i >= 0; $i--) {
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>foreach:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count - 1;
                foreach ($arr as $key => $item) {
                    $reversed[$i - $key] = $item;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count;
                while ($i >= 0) {
                    $i--;
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>?>
            </li>
            <li>do while:
                <?php
                $arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count;
                do {
                    $i--;
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                } while ($i >= 0);
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li> for:
                <?php
                $arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                for ($i = $count - 1; $i >= 0; $i--) {
                    if ($i > $count - 1) {
                        break;
                    }
                    $reversed[] = $arr[$i];
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>foreach:
                <?php

                $arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $reversed = [];
                for ($count = 0; $arr[$count]; $count++) ;
                $i = $count - 1;
                foreach ($arr as $key => $item) {
                    $reversed[$i - $key] = $item;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Reversed array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = $count - 1;
                while ($i >= 0) {
                    $reversed .= $str[$i];
                    $i--;
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Reversed string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li>do while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = $count - 1;
                do {
                    $reversed .= $str[$i];
                    $i--;
                } while ($i >= 0);
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Reversed string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li> for:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                for ($i = $count - 1; $i >= 0; $i--) {
                    $reversed .= $str[$i];
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Reversed string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li>foreach:
                <?php ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = 0;
                while ($i <= $count) {
                    $reversed .= strtolower($str[$i]);
                    $i++;
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Lower case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>';
                ?>
            </li>
            <li>do while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = 0;
                do {
                    $reversed .= strtolower($str[$i]);
                    $i++;
                } while ($i <= $count);
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Lower case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li> for:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                for ($i = 0; $i <= $count; $i++) {
                    $reversed .= strtolower($str[$i]);
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Lower case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li>foreach:
                <?php ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = 0;
                while ($i <= $count) {
                    $reversed .= strtoupper($str[$i]);
                    $i++;
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Upper case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>';
                ?>
            </li>
            <li>do while:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                $i = 0;
                do {
                    $reversed .= strtoupper($str[$i]);
                    $i++;
                } while ($i <= $count);
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Upper case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li> for:
                <?php
                $str = 'Hi I am Alex';
                $reversed = '';
                $count = 0;
                for ($count = 0; $str{$count}; $count++) ;
                for ($i = 0; $i <= $count; $i++) {
                    $reversed .= strtoupper($str[$i]);
                }
                echo 'Srting:';
                echo '<br>';
                echo $str;
                echo '<br>';
                echo 'Upper case string:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li>foreach:
                <?php ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                while ($i <= $count) {
                    $reversed[] .= strtolower($arr[$i]);
                    $i++;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Lower case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>do while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                do {
                    $reversed[] .= strtolower($arr[$i]);
                    $i++;
                } while ($i <= $count);
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Lower case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li> for:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                for ($i = 0; $i <= $count; $i++) {
                    $reversed[] .= strtolower($arr[$i]);
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Lower case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>foreach:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                foreach ($arr as $item) {
                    $reversed[$i] = strtolower($item);
                    $i++;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Lower case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>';
                ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                while ($i <= $count) {
                    $reversed[] .= strtoupper($arr[$i]);
                    $i++;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Upper case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>do while:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                do {
                    $reversed[] .= strtoupper($arr[$i]);
                    $i++;
                } while ($i <= $count);
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Upper case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li> for:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                for ($i = 0; $i <= $count; $i++) {
                    $reversed[] .= strtoupper($arr[$i]);
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Upper case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>'; ?>
            </li>
            <li>foreach:
                <?php
                $arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
                $reversed = [];
                $count = 0;
                for ($count = 0; $arr[$count]; $count++) ;
                $i = 0;
                foreach ($arr as $item) {
                    $reversed[$i] = strtoupper($item);
                    $i++;
                }
                echo '<pre>';
                print_r($arr);
                echo '</pre>';
                echo 'Upper case array:';
                echo '<pre>';
                print_r($reversed);
                echo '</pre>';
                ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $num = 12345678;
                $num = (string)$num;
                $reversed = '';
                $count = 0;
                for ($count = 0; $num{$count}; $count++) ;
                $i = $count - 1;
                while ($i >= 0) {
                    $reversed .= $num[$i];
                    $i--;
                }
                $reversed = (integer)$reversed;
                echo 'Number:';
                echo '<br>';
                echo $num;
                echo '<br>';
                echo 'Reversed number:';
                echo '<br>';
                echo $reversed;
                echo '<br>'; ?>
            </li>
            <li>do while:
                <?php
                $num = 12345678;
                $num = (string)$num;
                $reversed = '';
                $count = 0;
                for ($count = 0; $num{$count}; $count++) ;
                $i = $count - 1;
                do {
                    $reversed .= $num[$i];
                    $i--;
                } while ($i >= 0);
                $reversed = (integer)$reversed;
                echo 'Number:';
                echo '<br>';
                echo $num;
                echo '<br>';
                echo 'Reversed number:';
                echo '<br>';
                echo $reversed;
                echo '<br>';
                ?>
            </li>
            <li> for:
                <?php
                $num = 12345678;
                $num = (string)$num;
                $reversed = '';
                $count = 0;
                for ($count = 0; $num{$count}; $count++) ;
                for ($i = $count - 1; $i >= 0; $i--) {
                    $reversed .= $num[$i];
                }
                $reversed = (integer)$reversed;
                echo 'Number:';
                echo '<br>';
                echo $num;
                echo '<br>';
                echo 'Reversed number:';
                echo '<br>';
                echo $reversed;
                echo '<br>';
                ?>
            </li>
            <li>foreach:
                <?php ?>
            </li>
        </ul>
    </li>
    <li>
        <ul>
            <li>while:
                <?php
                $arr1 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $arr2 = $arr1;
                for ($count = 0; $arr2[$count]; $count++) ;
                $i = 0;
                while ($i<$count){
                    $key=0;
                    $j=0;
                    while ($j<$count){
                        if($arr2[$j]<$arr2[$i]){
                            $key=$arr2[$i];
                            $arr2[$i]=$arr2[$j];
                            $arr2[$j]=$key;
                        }
                        $j++;
                    }
                    $i++;
                }
                echo '<pre>';
                print_r($arr1);
                echo '</pre>';
                echo 'Sorted array:';
                echo '<pre>';
                print_r($arr2);
                echo '</pre>'; ?>
            </li>
            <li>do while:
                <?php
                $arr1 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $arr2 = $arr1;
                for ($count = 0; $arr2[$count]; $count++) ;
                $i = 0;
                do{
                    $key=0;
                    $j=0;
                    do{
                        if($arr2[$j]<$arr2[$i]){
                            $key=$arr2[$i];
                            $arr2[$i]=$arr2[$j];
                            $arr2[$j]=$key;
                        }
                        $j++;
                    } while ($j<$count);
                    $i++;
                } while ($i<$count);
                echo '<pre>';
                print_r($arr1);
                echo '</pre>';
                echo 'Sorted array:';
                echo '<pre>';
                print_r($arr2);
                echo '</pre>';
                ?>
            </li>
            <li> for:
                <?php
                $arr1 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $arr2 = $arr1;
                for ($count = 0; $arr2[$count]; $count++) ;
                for ($i = 0; $i < $count; $i++){
                    $key = 0;
                    for ($j = 0; $j< $count; $j++){
                        if($arr2[$j]<$arr2[$i]){
                            $key=$arr2[$i];
                            $arr2[$i]=$arr2[$j];
                            $arr2[$j]=$key;
                        }
                    }
                }
                echo '<pre>';
                print_r($arr1);
                echo '</pre>';
                echo 'Sorted array:';
                echo '<pre>';
                print_r($arr2);
                echo '</pre>';
                ?>
            </li>
            <li>foreach:
                <?php
                $arr1 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
                $arr2 = $arr1;
                for ($count = 0; $arr2[$count]; $count++) ;
                $i = 0;
                foreach ($arr2 as $item1){
                    $j = 0;
                    foreach ($arr2 as $item2){
                        if ($item2<$item1){
                            $key = $arr2[$i];
                            $arr2[$i] = $arr2[$j];
                            $arr2[$j] = $key;
                        }
                        $j++;
                    }
                    $i++;
                }
                echo '<pre>';
                print_r($arr1);
                echo '</pre>';
                echo 'Sorted array:';
                echo '<pre>';
                print_r($arr2);
                echo '</pre>';
                ?>
            </li>
        </ul>
    </li>
</ol>
</body>
</html>
