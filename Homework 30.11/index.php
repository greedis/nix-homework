<?php
$name = 'Ilya';
$age = 19;
$pi = pi();
$arr1 = ['alex', 'vova', 'tolya'];
$arr2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<ol>
    <li>My name - <?= $name?></li>
    <br>
    <li>My age - <?= $age?></li>
    <br>
    <li>Pi - <?= $pi?></li>
    <br>
    <li> Array 1:
        <pre>
            <?php print_r($arr1)?>
        </pre>
    </li>
    <li>Array 2:
        <pre>
            <?php print_r($arr2)?>
        </pre>
    </li>
    <li>Array 3:
        <pre>
            <?php print_r($arr3)?>
        </pre>
    </li>
    <li>Array 4:
        <pre>
            <?php print_r($arr4)?>
        </pre>
    </li>
</ol>
</body>
</html>